use std::path::PathBuf;
use std::fs::File;
use std::io::prelude::*;

extern crate clap;
use clap::{Arg, App};
use lazy_static::lazy_static;
use directories::ProjectDirs;
use toml::Value;

use crate::util;

pub const STORAGE_DIRECTORY: &str = "/tmp/buf";
pub const CONFIG_FILE_NAME: &str = "buf.toml";

/**
 * Describe the configuration resulting from the command-line arguments.
 */
#[derive(Debug)]
pub struct CliConfig {
    pub buffer_name: String,
    pub storage_dir: Option<String>,
    pub tty_detect: bool,
    pub list: bool,
    pub info: bool,
}

impl<'a, 'b> CliConfig {
    /**
     * Parse the command-line arguments, and return a structure describing the resulting
     * configuration.
     */
    pub fn new() -> CliConfig {
        let definition = Self::get_cli_definition();
        let matches = definition.get_matches();

        CliConfig {
            buffer_name: match matches.value_of("NAME") {
                Some(name) => s!(name),
                None       => s!(),
            },
            tty_detect: !matches.is_present("no-detect") && !matches.is_present("print"),
            list: matches.is_present("list"),
            info: matches.is_present("info"),
            storage_dir: matches.value_of("storage-dir").map(|s| s!(s)),
        }
    }

    /**
     * Return the command-line argument definition for clap.
     */
    fn get_cli_definition() -> App<'a, 'b> {
        lazy_static! {
            static ref STORAGE_DIR_HELP_MESSAGE: String = format!(
                "Storage directory to use (default: '{}')",
                STORAGE_DIRECTORY
            );
        }

        App::new("buf")
        .version("0.1")
        .about("Store data into buffer files.")
        .arg(Arg::with_name("print")
             .short("p")
             .long("print")
             .help("Print the given value (no auto-detection of stdin)"))
        .arg(Arg::with_name("no-detect")
             .short("n")
             .long("no-detect")
             .help("Alias for --print"))
        .arg(Arg::with_name("list")
             .short("l")
             .long("list")
             .help("List existing buffers"))
        .arg(Arg::with_name("info")
             .short("i")
             .long("info")
             .help("Show the absolute path to the given buffer. The buffer may not exist."))
        .arg(Arg::with_name("storage-dir")
             .short("s")
             .long("storage-dir")
             .takes_value(true)
             .help(&STORAGE_DIR_HELP_MESSAGE)
             .value_name("DIR"))
        .arg(Arg::with_name("NAME")
             .index(1)
             .help("Optional buffer name"))
    }
}


/**
 * Retrieve the proper config filename, depending on the current OS.
 */
fn get_config_file() -> Option<PathBuf> {
    ProjectDirs::from("buf", "buf", "buf").map(|proj_dirs| {
        let mut path = proj_dirs.config_dir().to_path_buf();
        path.push(CONFIG_FILE_NAME);
        path
    })
}

/**
 * Retrieve the storage directory by looking into different source, with the following priorities:
 * - Command-line argument --storage-dir
 * - Configuration file (typically $HOME/.config/buf/buf.toml in linux)
 * - Default value (/tmp/buf)
 */
fn get_storage_dir(cli_config: &CliConfig) -> String {
    if cli_config.storage_dir.is_some() {
        return cli_config.storage_dir.clone().unwrap();
    }

    fn get_storage_dir_from_config_file() -> Option<String> {
        let config_file = get_config_file()?;
        let mut file = File::open(config_file).ok()?;
        let mut toml_contents = String::new();
        file.read_to_string(&mut toml_contents).ok()?;
        let values = toml_contents.parse::<Value>().unwrap();
        let v = values.get("storage-dir")?.as_str()?.to_string();
        Some(v)
    }

    get_storage_dir_from_config_file()
        .unwrap_or_else(|| s!(STORAGE_DIRECTORY))
}


/**
 * Describe how the command should behave.
 */
pub enum CommandMode {
    /// Stores a buffer
    Store,
    /// Show content of a buffer
    Show,
    /// List existing buffers
    List,
    /// Show the path of a buffer
    Info,
}


/**
 * Final configuration.
 */
pub struct Config {
    pub buffer_name: String,
    pub storage_dir: String,
    pub command_mode: CommandMode,
}

impl Config {
    pub fn new() -> Config {
        let cli_config = CliConfig::new();

        let storage_dir = get_storage_dir(&cli_config);

        let command_mode =
            if cli_config.list {
                CommandMode::List
            } else if cli_config.info {
                CommandMode::Info
            } else if cli_config.tty_detect && !util::is_tty() {
                CommandMode::Store
            } else {
                CommandMode::Show
            };

        Config {
            buffer_name: cli_config.buffer_name,
            storage_dir,
            command_mode
        }
    }
}
