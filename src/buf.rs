
use std::path::PathBuf;
use std::fs;
use std::io::{Read, Write};

/**
 * Manage existing buffer files.
 */
pub struct Manager {
    storage_directory: String,
}

impl Manager {
    /**
     * Create a new Manager instance.
     */
    pub fn new(storage_directory: &str) -> Manager {
        Manager {
            storage_directory: s!(storage_directory),
        }
    }

    /**
     * Return a list of all existing buffer names.
     */
    pub fn list(&self) -> Result<impl Iterator<Item = String>, String> {
        let entries = fs::read_dir(&self.storage_directory)
            .map_err(|_| s!("Unable to read the storage directory"))?;

        let names = entries
            .filter_map(|result| result.ok())
            .filter_map(|e| {
                let filename = match e.file_name().into_string() {
                    Ok(s) => s,
                    Err(_) => return None,
                };
                if filename == "buf" {
                    return Some(s!());
                }
                if filename.starts_with("buf_") {
                    let stripped = unsafe { filename.get_unchecked(4..) };
                    return Some(s!(stripped));
                }
                None
            });

        Ok(names)
    }
}

/**
 * Represent a buffer.
 */
pub struct Buf {
    storage_directory: String,
    buffer_name: String,
}

impl Buf {
    /**
     * Create a new Buf instance.
     */
    pub fn new(storage_directory: &str, buffer_name: &str) -> Buf {
        Buf {
            storage_directory: s!(storage_directory),
            buffer_name: s!(buffer_name),
        }
    }

    /**
     * Create the storage directory if needed.
     */
    pub fn need_storage_directory(&self) -> Result<(), String> {
        match fs::create_dir_all(&self.storage_directory) {
            Ok(_) => Ok(()),
            Err(_) => Err(s!("Unable to create the storage directory")), // TODO: improve
        }
    }

    /**
     * Return the full path to the given buffer file.
     */
    pub fn get_filename(&self) -> PathBuf {
        let filename = match &*self.buffer_name {
            "" => s!("buf"),
            _  => format!("buf_{}", &self.buffer_name),
        };

        [ &self.storage_directory, &filename ].iter().collect()
    }

    /**
     * Read the data stored for the given buffer name.
     */
    pub fn get_data(&self) -> Result<String, String> {
        let buffer_filename = self.get_filename();

        // Open the buffer file:
        let mut buffer_file = match fs::File::open(&buffer_filename) {
            Ok(buffer_file) => buffer_file,
            Err(_) => return Err(s!("No such buffer")),
        };

        // Read the data:
        let mut data = s!();
        if let Err(_) = buffer_file.read_to_string(&mut data) {
            return Err(s!("Unable to read data from the buffer file."));
        }

        Ok(data)
    }

    /**
     * Store the given data in a buffer.
     */
    pub fn store_data(&self, data: &str) -> Result<(), String> {
        self.need_storage_directory()?;
        let buffer_filename = self.get_filename();

        // Open the buffer file:
        let mut buffer_file = match fs::File::create(&buffer_filename) {
            Ok(buffer_file) => buffer_file,
            Err(_) => return Err(s!("Unable to create the buffer file")),
        };

        // Write the data:
        if let Err(_) = buffer_file.write_all(data.as_bytes()) {
            return Err(s!("Unable to write data to the buffer file"));
        }

        Ok(())
    }
}
