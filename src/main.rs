#[macro_use] mod util;
mod config;
mod buf;

use config::Config;
use config::CommandMode;
use buf::{Buf, Manager};

/**
 * Create a new Buf instance, configured with the default storage directory.
 */
fn new_buf(config: &Config) -> Buf {
    Buf::new(&config.storage_dir, &config.buffer_name)
}

/**
 * Prints the content of the given buffer to stdout.
 */
fn show(config: &Config) -> Result<(), String> {
    let buf = new_buf(config);
    let data = buf.get_data()?;
    print!("{}", data);

    Ok(())
}

/**
 * Prints the full path to the given buffer to stdout.
 */
fn info(config: &Config) -> Result<(), String> {
    let buf = new_buf(config);
    let filename = buf.get_filename();
    println!("{}", filename.display());

    Ok(())
}

/**
 * Store data from stdin to a buffer.
 */
fn store_from_stdin(config: &Config) -> Result<(), String> {
    let data = util::read_all_stdin()?;
    let buf = new_buf(config);
    buf.store_data(&data)
}

/**
 * List existing buffers.
 */
fn list(config: &Config) -> Result<(), String> {
    let manager = Manager::new(&config.storage_dir);

    let buffer_names = manager.list()?
        .map(|name| match name.as_str() {
            "" => s!("(unnamed buffer)"),
            _ => name,
        });

    for buffer_name in buffer_names {
        println!("{}", buffer_name);
    }

    Ok(())
}

/**
 * Main program without global error handling.
 */
fn run() -> Result<(), String> {
    // Parse cli arguments, then setup a whole config description:
    let config = Config::new();

    match config.command_mode {
        CommandMode::Show  => show(&config),
        CommandMode::Store => store_from_stdin(&config),
        CommandMode::List => list(&config),
        CommandMode::Info => info(&config),
    }
}

/**
 * main
 */
fn main() {
    if let Err(message) = run() {
        eprintln!("{}", message);
        std::process::exit(1);
    }
}
