
extern crate libc;

use std::io;
use std::io::Read;

#[macro_export]
macro_rules! s {
    () => { String::new() };
    ($e:expr) => { String::from($e) };
}

/**
 * Detect if the standard input comes from a tty.
 */
pub fn is_tty() -> bool {
    (unsafe { libc::isatty(libc::STDIN_FILENO as i32) }) != 0
}

pub fn read_all_stdin() -> Result<String, String> {
    let mut data = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();

    match handle.read_to_string(&mut data) {
        Err(_) => Err(s!("Unable to read data from stdin")),
        Ok(_)  => Ok(data),
    }
}
